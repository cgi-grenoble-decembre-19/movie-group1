import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeriesComponent } from './containers/series/series.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SeriesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeriesRoutingModule { }
