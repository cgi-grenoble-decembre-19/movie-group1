import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiComponent } from './containers/ui/ui.component';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [UiComponent, HeaderComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [UiComponent, HeaderComponent]
})
export class UiModule { }
