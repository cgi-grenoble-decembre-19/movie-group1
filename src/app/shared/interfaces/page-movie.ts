import { MovieI } from './movie-i';

export interface PageMovie {

  results: MovieI[];
  total_results: number;
  dates: [];
  total_pages: number;

}
