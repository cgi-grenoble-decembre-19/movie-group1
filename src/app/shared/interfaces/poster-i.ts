export interface PosterI {
  id: number;
  backdrops: [
    {
      aspect_ratio: number;
      file_path: string;
      height: number;
      iso_639_1: number;
      vote_average: number;
      vote_count: number;
      width: number;
    }
  ];
  posters: [
    {
      aspect_ratio: number;
      file_path: string;
      height: number;
      iso_639_1: string;
      vote_average: number;
      vote_count: number;
      width: number;
    }
  ];
}
