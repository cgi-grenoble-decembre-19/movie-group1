import { Injectable } from '@angular/core';
import { MovieI } from 'src/app/shared/interfaces/movie-i';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { PageMovie } from 'src/app/shared/interfaces/page-movie';
import { PosterI } from 'src/app/shared/interfaces/poster-i';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) { }

  search(query: string) {

    return this.http.get<PageMovie>(`${environment.urlApi}/search/movie/?api_key=${environment.apiKey}&query=${encodeURIComponent(query)}`);
  }

  getLatest(pageNumber: number = 1) {
    return this.http.get<PageMovie>(`${environment.urlApi}/movie/latest?api_key=${environment.apiKey}&language=en-US&page=${pageNumber}`);
  }

  getNowPlaying(pageNumber: number = 1) {
    return this.http.get<PageMovie>(`${environment.urlApi}/movie/now_playing?api_key=${environment.apiKey}&language=en-US&page=${pageNumber}`);
  }


  getUpComming(pageNumber: number = 1) {
    return this.http.get<PageMovie>(`${environment.urlApi}/movie/upcoming?api_key=${environment.apiKey}&language=en-US& page=${pageNumber}`);
  }

}
