import { Component, OnInit, Input } from '@angular/core';
import { MovieI } from 'src/app/shared/interfaces/movie-i';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.scss']
})
export class ListMoviesComponent implements OnInit {
  @Input() movies: MovieI[];
  @Input() title = 'TITRE';

  constructor() { }

  ngOnInit() {
    console.log(this.movies);
  }

}
