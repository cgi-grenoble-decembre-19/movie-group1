import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { tap } from 'rxjs/operators';
import { MovieI } from 'src/app/shared/interfaces/movie-i';
import { SearchService } from 'src/app/shared/services/search.service';

@Component({
  selector: 'app-search-movies',
  templateUrl: './search-movies.component.html',
  styleUrls: ['./search-movies.component.scss']
})
export class SearchMoviesComponent implements OnInit {
  movies: MovieI[];


  constructor(private moviesService: MoviesService, private searchService: SearchService) { }

  ngOnInit(

  ) {
    // this.searchService.search$.value();
    this.moviesService.search(
      'iron man'
    ).pipe(tap(data => console.log(data))).subscribe((data) => this.movies = data.results);
  }

}
