import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { MovieI } from 'src/app/shared/interfaces/movie-i';
import { map, tap } from 'rxjs/operators';
import { PosterI } from 'src/app/shared/interfaces/poster-i';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  moviesLatest: MovieI[];
  moviesPlaying: MovieI[];
  moviesUpComming: MovieI[];

  nowPlaying = 'NOW PLAYING';
  upcoming = 'UPCOMING';

  constructor(private moviesService: MoviesService) {
  }

  ngOnInit() {
    console.log('MoviesComponent : ngOnInit');
    this.moviesService.getNowPlaying().pipe(tap(data => console.log(data))).subscribe((data) => this.moviesPlaying = data.results);
    this.moviesService.getUpComming().pipe(tap(data => console.log(data))).subscribe((data) => this.moviesUpComming = data.results);
  }

}
