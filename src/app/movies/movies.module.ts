import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './containers/movies/movies.component';
import { SearchMoviesComponent } from './containers/search-movies/search-movies.component';
import { ListMoviesComponent } from './components/list-movies/list-movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [MoviesComponent, SearchMoviesComponent, ListMoviesComponent],
  imports: [
    CommonModule,
    SharedModule,
    MoviesRoutingModule
  ]
})
export class MoviesModule { }
